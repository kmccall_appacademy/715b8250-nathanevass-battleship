
#Bonus on https://github.com/Th3Nathan/Battleship 


class BattleshipGame
  attr_reader :board, :player
  def initialize(player, board)
    @board = board 
    @player = player
  end 
  
  def play
    until game_over? 
      @current_player.play_turn 
    end 
    puts "its over, you win"
  end 
  
  def attack(position)
    @board[position] = :x
  end 
  
  def count
    board.count
  end 
  
  def game_over?
    board.won? 
  end 
  
  def play_turn 
    pos = player.get_play 
    attack(pos)
  end 
end


