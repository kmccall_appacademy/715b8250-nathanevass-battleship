
class Board
  
  DISPLAY = {nil => " ", :s => " ", :x => "X" }
  
  attr_accessor :grid 
  def initialize(grid = Board.default_grid)
    @grid = grid 
  end 
  
  def self.default_grid
    Array.new(10) {Array.new(10)}
  end 
  
  def count 
    @grid.flatten.count (:s)
  end 
  
  def [](pos)
    row, col = pos 
    @grid[row][col]
  end 
  
  def []=(pos, val)
    row, col = pos 
    @grid[row][col] = val
  end 
  
  def empty?(position = @grid)
    if position == @grid 
      grid.flatten.all? {|space| space == nil}
    else
      self[position] == nil
    end 
  end 
  
  def full? 
    grid.flatten.all? {|space| space != nil}
  end 
  
  def rows
    grid.length 
  end 
  
  def columns 
    grid[0].length 
  end 
  
  def place_random_ship 
    while true 
      raise "That spot is taken" if self.full? 
      row = rand(0...rows)
      col = rand(0...columns)
      if empty?([row, col])
        self[[row, col]] = :s 
        return 
      else 
        next
      end 
    end 
  end 
  
  def won? 
    grid.flatten.none? {|square| square == :s}
  end 
  
  def display
    grid.each_with_index do |row, i|
      print i 
      puts row.map{|sym| DISPLAY[sym]}
    end 
  end 
  
end 

