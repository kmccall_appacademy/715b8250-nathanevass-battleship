
class HumanPlayer
  attr_accessor :board 
  
  def initialize(board = Board.new)
    @board = board 
  end 
  
  def get_play 
    puts "What row would you like to attack"
    row = gets.chomp
    puts "What column would you like to attack"
    col = gets.chomp
    [row.to_i, col.to_i]
  end 
  
end
